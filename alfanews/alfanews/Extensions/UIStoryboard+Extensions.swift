//
//  UIStoryboard+Extensions.swift
//  alfanews
//
//  Created by Alexander Gorskih on 27/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static func webContentVC(for post: Post) -> WebContentViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WebView") as! WebContentViewController

        vc.post = post

        return vc
    }
}
