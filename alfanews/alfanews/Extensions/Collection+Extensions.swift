//
//  Collection+Extensions.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Iterator.Element? {
        return index >= startIndex && index < endIndex ? self[index] : nil
    }
}
