//
//  Array+Extensions.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

extension Array where Element : Equatable {

    func index(of element: Element?) -> Int? {
        guard element != nil else {
            return nil
        }

        return index(of: element!)
    }

    func after(item: Element?) -> Element? {
        guard let index = index(of: item) else {
            return nil
        }

        return self[safe: index + 1]
    }

    func before(item: Element?) -> Element? {
        guard let index = index(of: item) else {
            return nil
        }

        return self[safe: index - 1]
    }
}
