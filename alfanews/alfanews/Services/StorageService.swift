//
//  StorageService.swift
//  alfanews
//
//  Created by Alexander Gorskih on 27/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

// Could use CoreData as backing layer
class StorageService {
    private let defaults = UserDefaults.standard

    var posts: [Post]? {
        get {
            guard let titles = defaults.value(forKey: "postTitles") as? [String],
                  let urls = defaults.value(forKey: "postURLs") as? [String] else {
                   return nil
            }

            return zip(titles, urls).flatMap { Post(title: $0, url: URL(string: $1)!) }
        }

        set {
            let titles = newValue?.map { $0.title }
            let urls = newValue?.map { "\($0.url)" }

            defaults.setValue(titles, forKey: "postTitles")
            defaults.setValue(urls, forKey: "postURLs")
        }
    }
}
