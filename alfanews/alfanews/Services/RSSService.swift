//
//  RSSService.swift
//  alfanews
//
//  Created by Alexander Gorskih on 27/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation
import Alamofire
import Kanna

class RSSService {
    let url: URL

    init(url: URL) {
        self.url = url
    }

    func fetchPosts(with completionHandler: @escaping ([Post]) -> Void) {
        Alamofire.request(url).responseString {
            if let xml = $0.result.value,
               let doc = Kanna.XML(xml: xml, encoding: .utf8) {
                let path = "/rss[@version=\"2.0\"]/channel//item"
                let posts = doc.xpath(path, namespaces: nil).flatMap { (node) -> Post? in
                    guard let title = node.at_xpath("title")?.content,
                          let url = node.at_xpath("link")?.content,
                          let postURL = URL(string: url) else {
                        return nil
                    }

                    return Post(title: title, url: postURL)
                }

                completionHandler(posts)
            }
        }
    }
}
