//
//  SyncService.swift
//  alfanews
//
//  Created by Alexander Gorskih on 27/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

class SyncService {
    private var timer: Timer?
    private let interval: TimeInterval = 5 * 60

    func startSync(with handler: @escaping () -> Void) {
        stopSync()
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { _ in
            handler()
        }
    }

    func stopSync() {
        timer?.invalidate()
    }
}
