//
//  AppDelegate.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

let rssURL = URL(string: "https://alfabank.ru/_/rss/_rss.html?subtype=1&category=2&city=21")!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
