//
//  Post.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

struct Post: Equatable {
    let title: String
    let url: URL
}

func == (lhs: Post, rhs: Post) -> Bool {
    return lhs.url == rhs.url
}
