//
//  FeedFeedPresenter.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

class FeedPresenter: NSObject, FeedModuleInput, FeedViewOutput, FeedInteractorOutput {

    // MARK: - Public Properties

    weak var view: FeedViewInput!
    var interactor: FeedInteractorInput!
    var router: FeedRouterInput!

    var totalPosts: Int {
        return interactor.posts.count
    }

    // MARK: - Public Methods

    func post(at index: Int) -> Post {
        return interactor.posts[index]
    }

    func viewIsReady() {
        if interactor.posts.isEmpty {
            update()
        }
    }

    func willUpdateFeed() {
        view.willUpdateFeed()
    }

    func didUpdateFeed() {
        view.didUpdateFeed()
    }

    func selectPost(at index: Int) {
        router.navigate(to: index, in: interactor.posts)
    }

    func update() {
        interactor.update()
    }
}
