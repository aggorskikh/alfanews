//
//  FeedFeedInteractorInput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

protocol FeedInteractorInput {
    var posts: [Post] { get }

    func update()
}
