//
//  FeedFeedInteractor.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

class FeedInteractor: FeedInteractorInput {

    weak var output: FeedInteractorOutput!
    private var rssService: RSSService!
    private var rssSyncService: SyncService!
    private var storageService: StorageService!

    var posts: [Post] {
        return storageService.posts ?? []
    }

    func update() {
        output.willUpdateFeed()
        rssService.fetchPosts { [weak self] posts in
            self?.storageService.posts = posts
            self?.output.didUpdateFeed()
        }
    }

    func configure(with rssService: RSSService, and syncService: SyncService, storage storageService: StorageService) {
        self.rssService = rssService
        self.rssSyncService = syncService
        self.storageService = storageService
        startBackgroundUpdate()
    }

    private func startBackgroundUpdate() {
        rssSyncService.startSync { [weak self] in
            self?.update()
        }
    }

    deinit {
        rssSyncService.stopSync()
    }
}
