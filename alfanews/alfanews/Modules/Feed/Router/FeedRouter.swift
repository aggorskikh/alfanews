//
//  FeedFeedRouter.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation
import ViperMcFlurry

class FeedRouter: NSObject, FeedRouterInput {
    var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    weak var moduleOutput: FeedModuleInput!

    func navigate(to index: Int, in posts: [Post]) {
        transitionHandler.openModule!(usingSegue: "OpenPostSegue").thenChain { [weak self] input in
            guard let postModuleInput = input as? PostModuleInput else {
                fatalError("Invalid module type")
            }

            postModuleInput.configure(with: posts, selectedAt: index)

            return self?.moduleOutput
        }
    }
}
