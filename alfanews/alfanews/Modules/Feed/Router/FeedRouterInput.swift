//
//  FeedFeedRouterInput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation
import ViperMcFlurry

protocol FeedRouterInput: RamblerViperModuleInput {
    func navigate(to index: Int, in posts: [Post])
}
