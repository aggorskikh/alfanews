//
//  FeedFeedInitializer.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class FeedModuleInitializer: NSObject {

    @IBOutlet weak var feedViewController: FeedViewController!

    override func awakeFromNib() {
        let configurator = FeedModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: feedViewController)
    }

}
