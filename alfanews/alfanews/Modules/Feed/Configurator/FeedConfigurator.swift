//
//  FeedFeedConfigurator.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class FeedModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? FeedViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: FeedViewController) {
        let router = FeedRouter()
        router.transitionHandler = viewController

        let presenter = FeedPresenter()
        presenter.view = viewController
        presenter.router = router
        router.moduleOutput = presenter

        let interactor = FeedInteractor()
        interactor.output = presenter

        let rssService = RSSService(url: rssURL)
        let syncService = SyncService()
        let storageService = StorageService()
        interactor.configure(with: rssService, and: syncService, storage: storageService)

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
