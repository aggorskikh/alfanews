//
//  FeedFeedViewOutput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

protocol FeedViewOutput {
    var totalPosts: Int { get }

    func viewIsReady()
    func post(at index: Int) -> Post
    func selectPost(at index: Int)
    func update()
}
