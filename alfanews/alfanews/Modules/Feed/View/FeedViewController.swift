//
//  FeedFeedViewController.swift
//  alfanews
//
//  Created by Alexander Gorskih on 25/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController, FeedViewInput {

    // MARK: - Public Properties

    var output: FeedViewOutput!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl?.addTarget(self, action: #selector(update), for: .valueChanged)
        output.viewIsReady()
    }

    // MARK: - Public Methods

    func willUpdateFeed() {
    }

    func didUpdateFeed() {
        tableView.reloadData()
        refreshControl?.endRefreshing()
    }

    // MARK: - Internal methods

    internal func update() {
        output.update()
    }
}

// MARK: - UITableViewDataSource
extension FeedViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.totalPosts
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeedItem") else {
            fatalError("No Cell with FeedItem identifier")
        }

        let post = output.post(at: indexPath.row)

        cell.textLabel?.text = "\(post.title)"

        return cell
    }
}

// MARK: - UITableViewDelegate
extension FeedViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        output.selectPost(at: indexPath.row)
    }
}
