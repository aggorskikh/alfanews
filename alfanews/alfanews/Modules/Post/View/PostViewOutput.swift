//
//  PostViewOutput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

protocol PostViewOutput {
    var totalPosts: Int { get }

    func viewIsReady()
    func post(after post: Post) -> Post?
    func post(before post: Post) -> Post?
    func index(of post: Post) -> Int?
}
