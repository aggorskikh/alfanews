//
//  PostViewInput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

protocol PostViewInput: class {
    func setupInitialState(with post: Post)
}
