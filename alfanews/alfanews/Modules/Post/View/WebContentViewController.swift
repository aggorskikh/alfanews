//
//  WebContentViewController.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/17.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class WebContentViewController: UIViewController {

    // MARK: - IBOutlets: Views & Controllers

    @IBOutlet weak var webView: UIWebView!

    // MARK: - Public Properties

    var post: Post! {
        didSet {
            let url = post?.url ?? URL(string: "about:blank")!
            let request = URLRequest(url: url)

            _ = view
            webView.loadRequest(request)
        }
    }
}
