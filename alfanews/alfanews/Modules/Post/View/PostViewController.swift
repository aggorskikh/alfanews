//
//  PostViewController.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit
import ViperMcFlurry
import Social

class PostViewController: UIViewController, PostViewInput {

    // MARK: - IBOutlets: Views & Controllers

    @IBOutlet weak var containerView: UIView!

    // MARK: - Public Properties

    var output: PostViewOutput!

    // MARK: - Private Properties

    private weak var pageVC: UIPageViewController!
    fileprivate var currentVC: WebContentViewController?
    fileprivate var nextVC: WebContentViewController?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    func setupInitialState(with post: Post) {
        currentVC = UIStoryboard.webContentVC(for: post)
        setupPageVC()
    }

    // MARK: - Segue

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dvc = segue.destination as? UIPageViewController, segue.identifier == "EmbedPageVC" {
            pageVC = dvc
        }
    }

    // MARK: - IBActions

    @IBAction func share(_ sender: Any) {
        let facebookShare = shareToFacebookAction()
        let tweeterShare = shareToTweeterAction()
        let cancel = cancelAction()
        let shareSheet = UIAlertController(title: "Поделиться",
                                           message: nil,
                                           preferredStyle: .actionSheet)

        shareSheet.addAction(facebookShare)
        shareSheet.addAction(tweeterShare)
        shareSheet.addAction(cancel)

        present(shareSheet, animated: true, completion: nil)
    }

    // MARK: - Private Methods

    private func shareToFacebookAction() -> UIAlertAction {
        return UIAlertAction(title: "Facebook", style: .default) { [weak self] _ in
            if let socialVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
                socialVC.setInitialText("Смотри какая классная новость")
                socialVC.add(self?.currentVC?.post.url)
                self?.present(socialVC, animated: true, completion: nil)
            }
        }
    }

    private func shareToTweeterAction() -> UIAlertAction {
        return UIAlertAction(title: "Tweeter", style: .default) { [weak self] _ in
            if let socialVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
                socialVC.setInitialText("Смотри какая классная новость")
                socialVC.add(self?.currentVC?.post.url)
                self?.present(socialVC, animated: true, completion: nil)
            }
        }
    }

    private func cancelAction() -> UIAlertAction {
        return UIAlertAction(title: "Отмена", style: .cancel)
    }

    private func setupPageVC() {
        pageVC.dataSource = self
        pageVC.delegate = self
        pageVC.setViewControllers([currentVC!],
                                  direction: .forward,
                                  animated: true,
                                  completion: nil)
    }
}

// MARK: - UIPageViewControllerDelegate
extension PostViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            willTransitionTo pendingViewControllers: [UIViewController]) {
        nextVC = pendingViewControllers.first as? WebContentViewController
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentVC = nextVC
        }

        nextVC = nil
    }
}

// MARK: - UIPageViewControllerDataSource
extension PostViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let contentVC = viewController as! WebContentViewController

        if let nextPost = output.post(after: contentVC.post) {
            return UIStoryboard.webContentVC(for: nextPost)
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let contentVC = viewController as! WebContentViewController

        if let beforePost = output.post(before: contentVC.post) {
            return UIStoryboard.webContentVC(for: beforePost)
        }

        return nil
    }

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return output.totalPosts
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let post = currentVC?.post {
            return output.index(of: post) ?? 0
        }

        return 0
    }
}
