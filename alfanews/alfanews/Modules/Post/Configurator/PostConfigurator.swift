//
//  PostModuleConfigurator.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class PostModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? PostViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: PostViewController) {
        let router = PostRouter()
        router.transitionHandler = viewController

        let presenter = PostPresenter()
        presenter.view = viewController
        presenter.router = router

        viewController.output = presenter
        viewController.moduleInput = presenter

        let interactor = PostInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
    }

}
