//
//  PostInitializer.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import UIKit

class PostModuleInitializer: NSObject {

    @IBOutlet weak var newsViewController: PostViewController!

    override func awakeFromNib() {
        let configurator = PostModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: newsViewController)
    }

}
