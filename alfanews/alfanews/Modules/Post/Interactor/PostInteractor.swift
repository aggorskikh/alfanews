//
//  PostInteractor.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

class PostInteractor: PostInteractorInput {
    var posts = [Post]()

    weak var output: PostInteractorOutput!

    func configure(with posts: [Post]) {
        self.posts = posts
    }
}
