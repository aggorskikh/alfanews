//
//  PostInteractorInput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation

protocol PostInteractorInput {
    var posts: [Post] { get }

    func configure(with posts: [Post])
}
