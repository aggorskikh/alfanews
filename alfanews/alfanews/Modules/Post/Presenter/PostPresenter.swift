//
//  PostPresenter.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import Foundation
import ViperMcFlurry

class PostPresenter: NSObject, PostModuleInput, PostViewOutput, PostInteractorOutput {

    // MARK: - Public Properties

    weak var view: PostViewInput!
    var interactor: PostInteractorInput!
    var router: PostRouterInput!
    var feedModuleOutput: RamblerViperModuleOutput!
    var totalPosts: Int {
        return interactor.posts.count
    }

    // MARK: - Private Properties

    fileprivate var initialPost: Post!

    // MARK: - Public Methods

    func viewIsReady() {
        view.setupInitialState(with: initialPost)
    }

    func post(after post: Post) -> Post? {
        return interactor.posts.after(item: post)
    }

    func post(before post: Post) -> Post? {
        return interactor.posts.before(item: post)
    }

    func index(of post: Post) -> Int? {
        return interactor.posts.index(of: post)
    }

    func configure(with posts: [Post], selectedAt index: Int) {
        initialPost = posts[index]
        interactor.configure(with: posts)
    }

    func setModuleOutput(_ moduleOutput: RamblerViperModuleOutput!) {
        feedModuleOutput = moduleOutput as RamblerViperModuleOutput
    }
}
