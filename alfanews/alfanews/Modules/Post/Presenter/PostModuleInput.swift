//
//  PostModuleInput.swift
//  alfanews
//
//  Created by Alexander Gorskih on 22/03/2017.
//  Copyright © 2017 Noveo. All rights reserved.
//

import ViperMcFlurry

protocol PostModuleInput: class, RamblerViperModuleInput {
    func configure(with posts: [Post], selectedAt index: Int)
}
